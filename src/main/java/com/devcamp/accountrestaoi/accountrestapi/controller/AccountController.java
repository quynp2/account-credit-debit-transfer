package com.devcamp.accountrestaoi.accountrestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.accountrestaoi.accountrestapi.Account;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AccountController {
    @GetMapping("/accounts")
    public ArrayList<Account> AccountList() {
        ArrayList<Account> AccountList = new ArrayList<Account>();

        Account newAccount01 = new Account("ACB Da Nang", "Ngo Thi Hoang Nghia", 12000);
        Account newAccount02 = new Account("ACB Ho Chi Minh", "Nguyen Phuc Quy", 24000);
        Account newAccount03 = new Account("ACB Ha Noi", "Daisy Nguyen", 36000);

        newAccount01.credit(3000);
        newAccount01.debit(10000);
        newAccount01.transferTo(newAccount02, 1000);

        newAccount02.credit(1000);
        newAccount02.debit(10000);
        newAccount02.transferTo(newAccount03, 2000);

        newAccount03.credit(1000);
        newAccount03.debit(10000);
        newAccount03.transferTo(newAccount01, 4000);

        AccountList.add(newAccount01);
        AccountList.add(newAccount02);
        AccountList.add(newAccount03);

        return AccountList;
    }

}
